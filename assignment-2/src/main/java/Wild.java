class Wild extends Animal {

    Wild(String name, int length) {
        super(name, length);
        this.place = "outdoor";

        //untuk menentukan cage code dari setiap hewan
        if (this.getLength() < 75) {
            this.cageCode = 'A';
        } else if (this.getLength() <= 90) {
            this.cageCode = 'B';
        } else {
            this.cageCode = 'C';
        }
    }

}