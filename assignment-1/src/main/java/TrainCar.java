public class TrainCar {
	public WildCat cat;
	public TrainCar next;
	public final static  double EMPTY_WEIGHT = 20;
	
	
	public TrainCar (WildCat cat){
		this.cat = cat;
	}
	public TrainCar (WildCat cat, TrainCar next){
		this.cat = cat;
		this.next = next;
	}
	public double computeTotalWeight(){
		if (next == null) {
			return EMPTY_WEIGHT + cat.weight;
		}
		else{
			return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
		}
	}
	
	public double computeTotalMassIndex(){
		if (next == null) {
			return cat.computeMassIndex();
		}
		else {
			return cat.computeMassIndex() + next.computeTotalMassIndex();
		}
	}
	
	public void printCar(){
		System.out.printf("--(%s)  " , cat.name);
		if (next == null) {
			System.out.println("");
		}
		else {
			next.printCar();
		}
	}
}


