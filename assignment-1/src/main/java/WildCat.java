public class WildCat {
	
	public String name;
	public double weight;
	public double length;
	
	
	public WildCat(String name, double weight, double length){
		this.name= name;
		this.weight = weight;
		this.length = length;
	}	
	public double computeMassIndex(){
		double bmi = weight / ((length/100) * (length/100));
		return bmi;
		
	}	
}

